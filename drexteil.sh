#!/bin/bash

# my sat ip receiver does freeze regulary. So this is a script to restart it every night by 
# switching it off and on with a WLAN power plug and to do this also, when you connot ping it any more

# the ip of the sonoff switch or whatever
ROSOFF_IP="192.168.x.x"
# the ip to check for ping
DREXTEIL_IP="192.168.y.y."
# the mail address to inform about a failed ping
MAIL="foo@example.com"
# at this time we do a regular rosoff
ROSOFF_TIME="0400"
# we check every 30 seconds
LOOP_SLEEP=30

LAST_TIME=""

function rosoff {
    curl -s http://$ROSOFF_IP/off
    sleep 2
    curl -s http://$ROSOFF_IP/on
    sleep 300
}

while true
do
    # check the time
    if [ `date +%H%M` -eq $ROSOFF_TIME ]
    then
        if [ `date +%d%m%y` -ne $LAST_TIME ]
        then
           LAST_TIME=`date +%d%m%y`
           # ok, we have to reset
           rosoff
        fi
    fi
    # thats a good situation to go for the loop sleep
    sleep $LOOP_SLEEP
    ping -c 1 $DREXTEIL_IP
    if [ $? -ne 0 ]
    then
        # ok, we are down.
        mail -s 'drexteil restarted' $MAIL <<< 'restarted the drexteil at '`date`
        rosoff
    fi
done
