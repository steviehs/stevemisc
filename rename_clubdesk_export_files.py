#!/usr/bin/env python3



import argparse
import os

def process_name(filebytes):
    # print(filebytes)
    filebytes = filebytes.strip() # remove whitespace at beginning and end
    filebytes = filebytes.rstrip(b'.') # remove . at the end
    replacements = {
        b'\x2b\x2b': b'\xc3\xbc',  # ü
        b'\x2b\xf1': b'\xc3\xa4',  # ä
        b'\x2b\xc2': b'\xc3\xb6',  # ö
        b'o\xa6\xea': b'\xc3\xb6',  # ö
        b'\x2b\xe4': b'\xc3\x84',  # Ä
        b'A\xa6\xea': b'\xc3\x84',  # Ä
        b'\x2b\xa3': b'\xc3\x9c',  # Ü
        b'U\xa6\xea': b'\xc3\x9c',  # Ü
        b'\x2b\xfb': b'\xc3\x96',  # Ö
        b'\x2b\x83': b'\xc3\x9f',  # ß
        b'\xd4\xe9\xbc': b'\xe2\x82\xac', # €
        b'a\xa6\xea' : b'\xc3\xa4', # ä
        b'u\xa6\xea' : b'\xc3\xbc', # ü
        b'-\xba' : b'\xc2\xa7', # §
        b'\xd4\xc7\xaa' : b'', # dunno, what diz waz
        b'\xd4\xc7\xf4' : b'-', # long hyphen
        b'e\xa6\xfc' : b'\xc3\xa9', # é
        b'\xc3\xc7' : b'-', # this was |
        b':' : b'-', 
        b'?' : b'',
        b'>' : b'-',
        b'<' : b'-',
        b'|' : b'-',
        }
    for old, new in replacements.items():
        filebytes = filebytes.replace(old, new)
    return filebytes

def process_files(directory_path, none = True):
    renamed = False
    for root, dirs, files in os.walk(bytes(directory_path,"latin1")):
        for dir_name in dirs:
            current_path = os.path.join(root, dir_name)
            new_name = process_name(dir_name)
            if new_name != dir_name:
                if none:
                    print("Would change dir: ",repr(current_path)[2:-1]," -> ", new_name.decode('UTF-8'))
                else:
                    new_path = os.path.join(root, new_name)
                    os.rename(current_path, new_path)
                    renamed = True
                    print("Renamed dir: ",repr(current_path)[2:-1]," -> ", new_name.decode('UTF-8'))

        for file_name in files:
            current_path = os.path.join(root, file_name)
            new_name = process_name(file_name)
            if new_name != file_name:
                if none:
                    print("Would change file: ",repr(current_path)[2:-1]," -> ", new_name.decode('UTF-8'))
                else:
                    new_path = os.path.join(root, new_name)
                    os.rename(current_path, new_path)
                    renamed = True
                    print("Renamed file: ",repr(current_path)[2:-1]," -> ", new_name.decode('UTF-8'))
    return renamed

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Converts ClubDesk export file names to UTF-8"
    )
    parser.add_argument("-n", "--none", help="no action, do not rename", action="store_true")
    parser.add_argument('directory', help="folder to convert")
    args = parser.parse_args()
    loop_count = 0
    while process_files(args.directory, args.none) and loop_count < 9:
        loop_count += 1
        print("Rename loop:",loop_count)

    print("Ready, processed dir in loops:",loop_count)

if __name__ == "__main__":
    main()

