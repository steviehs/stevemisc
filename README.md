# stevemisc

Miscellaneous stuff which helps me and perhaps you?

`drexteil.sh` is a script for restarting a device which is not pingable anymore just by switching off and on a wifi plug.

`delete_alexa_devices.robot` is a robot script for deleting all smart home devices from your alexa account as it is not possible to use the delete all button. You can start it  with `robot -v USERNAME:your_amazon_account -v PASSWORD:your_amazon_password delete_alexa_devices.robot`

`rename_clubdesk_export_files.py` is a python script, which renames the strange zip exports to right UTF-8 filenames

